<!---
Please see filled-in example here: 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/examples/feature_request
--->

| Domain | Importance/Impact | Accountable |
| :------: | :------: | :------: |
| Security/UI/service etc | High/Mid/Low | @gitlab_id |

## Value
<!--- 
General, high level description for this new feature change / improvement of the Oniro ecosystem
For example, "With this new ABCD_feature, the Oniro booting will be reduced by 10% of the current booting time in the Linux port on RPI4b board"
--->

## Description
<!--- 
More detailed description of this new feature change
--->

## In scope
<!--- 
Optional: What is expected to be implemented/provided within this new feature
--->

## Out of Scope
<!--- 
Optional: What is not expected from this new feature (in case of doubts, ambiguities etc)
--->

## Acceptance Criteria
<!--- 
Optional: How to measure the success of this delivery?
--->

<!--- Please don't edit after this point --->
/label ~"State::New"
/label ~"Type::NewFeature"
/draft
